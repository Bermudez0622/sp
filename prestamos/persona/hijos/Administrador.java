package prestamos.persona.hijos;

//Paquetes Locales
import prestamos.persona.Persona;


public class Administrador extends Persona{

    //Atributos
    public static final long VALORMAXIMOPRESTAR = 2147483647 - 10000000;
    Cliente cliente;

    //Metodos
    //Constructor
    public Administrador(long cedula, String primerNombre, String primerApellido, String segundoApellido) {
        super(cedula, primerNombre, primerApellido, segundoApellido);
    }

    //Metodo para aprobar o negar Prestamos al cliente
    private void aprobacionPrestamo(){
        long var=0;
        if (cliente.isValidado()){
            for(int i=0;i<cliente.numPrestamos();i++){
                if(var+ cliente.getPrestamos().elementAt(i).getValorPrestamo()< VALORMAXIMOPRESTAR){
                    cliente.getPrestamos().elementAt(i).setAprobacion(true);
                    var+= cliente.getPrestamos().elementAt(i).getValorPrestamo();
                }
            }
        }
    }

    //Metodo para aprobar o negar la posibilidad de pedir prestamos al Cliente
    public void validacionCliente(boolean reportado){
        cliente.setValidacion(cliente.getEdad() >= 18 && !reportado);
        cliente.setReportado(reportado);
        this.aprobacionPrestamo();
    }

    //Metodo para crear el Cliente
    public void setNewCliente(char sexo, int edad, long telefono) {
        cliente = new Cliente(getCedula(), getPrimerNombre(), getPrimerApellido(), getSegundoApellido(), sexo, edad, telefono);
    }

    //Metodo get para cliente
    public Cliente getCliente() {
        return cliente;
    }
    
}