package prestamos.persona.hijos;

//Librerias
import java.util.Vector;

import javax.swing.JOptionPane;

//Paquetes Locales
import prestamos.persona.Persona;
import prestamos.prestamo.Prestamo;

public class Cliente extends Persona{

    //atributos
    private char sexo;
    private Vector <Prestamo> prestamos = new Vector<Prestamo>(0,1);
    private int edad;
    private long telefono;
    private boolean reportado, validacion;

    //metodos
    //Constructor
    public Cliente(long cedula, String primerNombre, String primerApellido,  String segundoApellido, char sexo, int edad, long telefono) {
        super(cedula, primerNombre, primerApellido, segundoApellido);
        this.sexo = sexo;
        this.edad = edad;
        this.telefono = telefono;
    }

    //Metodo get para validacion
    public boolean isValidado() {
        return validacion;
    }

    //Metodo get para edad
    public int getEdad() {
        return edad;
    }

    //Metodo set para validacion
    public void setValidacion(boolean validacion) {
        this.validacion = validacion;
    }

    //Metodo set para reportado
    public void setReportado(boolean reportado) {
        this.reportado = reportado;
    }

    //Metodo get para Vector prestamos
    public Vector<Prestamo> getPrestamos(){
        return this.prestamos;
    }

    //Metodo get para cantidad de Prestamos
    public int numPrestamos(){
        return this.prestamos.size();
    }

    //Metodo get para telefono
    public long getTelefono() {
        return this.telefono;
    }

    //Metodo set para agregar un nuevo prestamo
    public void setNewPrestamo(long valorPrestamo, int cuotas) {
        prestamos.add( new Prestamo(prestamos.size(), valorPrestamo, cuotas));
    }

    //Metodo para mostrar la informacion del Persona
    @Override
    public void mostrar() {
        JOptionPane.showMessageDialog(
            null,
            "Cedula: " + getCedula() +
            "\nNombre: " + getPrimerNombre() + " " + getPrimerApellido() + " " + getSegundoApellido() +
            "\nSexo: " + sexo +
            "\nEdad: " + edad +
            "\nTelefono: " + telefono
        );
        if(!validacion) {
            JOptionPane.showMessageDialog(
                null,
                "El usuario " + getPrimerNombre() + " " + getPrimerApellido() + " " + getSegundoApellido() +
                "\nNo puede pedir prestamos"
            );
        } else {
            for(int i = 0; i < prestamos.size(); ++i) {
                prestamos.elementAt(i).mostrar();
            }
        }
    }

}
    
