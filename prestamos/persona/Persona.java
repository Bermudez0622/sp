package prestamos.persona;

import javax.swing.JOptionPane;

public abstract class Persona{

    //Atributos
    long cedula;
    String primerNombre, primerApellido, segundoApellido;

    //Metodos
    //Constructor
    public Persona(long cedula, String primerNombre, String primerApellido, String segundoApellido) {
        this.cedula = cedula;
        this.primerNombre = primerNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
    }

    //Metodo get para cedula
    public long getCedula() {
        return cedula;
    }

    //Metodo get para primerNombre
    public String getPrimerNombre() {
        return primerNombre;
    }

    //Metodo get para primerApellido
    public String getPrimerApellido() {
        return primerApellido;
    }

    //Metodo get para segundoApellido
    public String getSegundoApellido() {
        return segundoApellido;
    }

    //Metodo para mostrar la informacion del Persona
    public void mostrar() {
        JOptionPane.showMessageDialog(
            null,
            "Cedula: " + cedula +
            "\nNombre: " + primerNombre + " " + primerApellido + " " + segundoApellido
        );
    }

}