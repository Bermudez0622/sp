package prestamos.prestamo;
//Librerias
import java.util.Calendar;
import java.util.Vector;

import javax.swing.JOptionPane;

import java.util.Locale;
import java.text.NumberFormat;
public class Prestamo {
    //Atributos
    private int numeroPrestamo;
    private boolean aprobacion;
    private long valorPrestamo;
    private Calendar fechaAutorizacionPrestamo = Calendar.getInstance(), fechaEntregaPrestamo = Calendar.getInstance();
    private Vector <Cuota> cuotas = new Vector <Cuota>(0, 0);
    //Metodos
    //Constructor
    public Prestamo(int numeroPrestamo, long valorPrestamo, int cuotas) {
        this.numeroPrestamo = numeroPrestamo;
        this.valorPrestamo = (valorPrestamo > 2000000) ? (long)(valorPrestamo - valorPrestamo * 0.05) : valorPrestamo;
        this.cuotas.setSize(cuotas);
    }
    //Metodo encargado de asignar las fechas y costos de cada cuota
    public void asignacionFechas() {
        Calendar fecha = Calendar.getInstance();
        int tamanno = this.cuotas.size();
        for(int i = 0; i < tamanno; ++i) {
            if(i == 0) {
                fecha.set(
                    this.fechaEntregaPrestamo.get(this.fechaEntregaPrestamo.YEAR),
                    this.fechaEntregaPrestamo.get(this.fechaEntregaPrestamo.MONTH),
                    this.fechaEntregaPrestamo.get(this.fechaEntregaPrestamo.DATE)
                );
            } else {
                fecha.set(
                    this.cuotas.elementAt(i - 1).getFecha().get(this.cuotas.elementAt(i - 1).getFecha().YEAR),
                    this.cuotas.elementAt(i - 1).getFecha().get(this.cuotas.elementAt(i - 1).getFecha().MONTH),
                    this.cuotas.elementAt(i - 1).getFecha().get(this.cuotas.elementAt(i - 1).getFecha().DATE)
                );
            }
            fecha.add(fecha.DATE, 30);
            this.cuotas.add(i, new Cuota(fecha, this.valorPrestamo/tamanno));
        }
        this.cuotas.setSize(tamanno);
    }

    //Metodo encargado de mostrar la informacion del prestamo
    public void mostrar() {
        if(this.aprobacion) {
            String cuotas = "";
            for(int i = 0; i < this.cuotas.size(); ++i) {
                cuotas += this.cuotas.elementAt(i).mostrar();
            }
            JOptionPane.showMessageDialog(
                null,
                "Id: " + this.numeroPrestamo +
                "\nValor: " + NumberFormat.getCurrencyInstance(Locale.US).format(this.valorPrestamo) +
                "\nAutorizacion: " + Prestamo.imprimirFecha(this.fechaAutorizacionPrestamo) +
                "\nEntrega: " + Prestamo.imprimirFecha(this.fechaEntregaPrestamo) +
                ((this.cuotas.size() > 1) ? "\nCuotas: " : "\nCuota: ") + this.cuotas.size() +
                "\n" + cuotas
            );
        } else {
            JOptionPane.showMessageDialog(
                null,
                "Id: " + this.numeroPrestamo +
                "\nValor: " + NumberFormat.getCurrencyInstance(Locale.US).format(this.valorPrestamo) +
                "\nPrestamo Negado"
            );
        }
    }

    //Metodo set para aprobacion
    public void setAprobacion(boolean aprobacion) {
        this.aprobacion = aprobacion;
        this.fechaAutorizacionPrestamo.set(
            this.fechaAutorizacionPrestamo.get(Calendar.YEAR),
            this.fechaAutorizacionPrestamo.get(Calendar.MONTH),
            this.fechaAutorizacionPrestamo.get(Calendar.DATE)
        );
        this.fechaEntregaPrestamo.add(this.fechaAutorizacionPrestamo.DATE, 5);
        this.asignacionFechas();
    }

    //Metodo encargado de dar un formato a la salida de texto para fechas
    public static String imprimirFecha(Calendar fecha) {
        String mes[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "junio", "Julio", "Agosto", "Septimbre", "Octubre", "Noviembre", "Diciembre"};
        return fecha.get(fecha.DATE) + " de " + mes[fecha.get(fecha.MONTH)] + " del " + fecha.get(fecha.YEAR);
    }

    //Metodo para dar estilos a los precios en clases externas
    public static String imprimirPrecio(long precio) {
        return NumberFormat.getCurrencyInstance(Locale.US).format(precio);
    }

    //Metodo get para valorPrestamo
    public long getValorPrestamo(){
        return valorPrestamo;
    }

}