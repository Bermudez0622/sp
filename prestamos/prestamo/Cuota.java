package prestamos.prestamo;

//Librerias
import java.util.Calendar;
import java.util.Locale;
import java.text.NumberFormat;

public class Cuota {

    //Atributos
    private Calendar fechaPagoCuota = Calendar.getInstance();
    private long valorCuota;

    //Metodos

    //Constructor
    public Cuota(Calendar fecha, long valorCuota) {
        this.fechaPagoCuota.set(
            fecha.get(fecha.YEAR),
            fecha.get(fecha.MONTH),
            fecha.get(fecha.DATE)
            );
        this.valorCuota = valorCuota;
    }

    //Metodo para mostrar la informacion de la cuota
    public String mostrar() {
        String cuota = "";
        cuota += "---------------------------------------------\n";
        cuota += "\tFecha: " + Prestamo.imprimirFecha(this.fechaPagoCuota) + "\n";
        cuota += "\tValor: " + NumberFormat.getCurrencyInstance(Locale.US).format(this.valorCuota) + "\n";
        cuota += "---------------------------------------------\n";
        return cuota;
    }

    //Metodo get para fechaPagoCuota
    public Calendar getFecha() {
        return this.fechaPagoCuota;
    }
}