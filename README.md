# Sistema de Prestamos
Proyecto Final

Esta es la culminacion del espacio academico Programacion Orientada a Objetos.

Este proyecto consiste en una aplicacion para el registro de préstamos en un Banco.

Que ademas:

** Muestra los datos completos del préstamo, incluyendo la fecha de entrega y las fechas de pago de las cuotas.

** Se debe muestra los nombres y apellidos de las personas que realizaron prestamos al igual que el monto total por sexo.

Proyecto que cumple con todos los requisistos ya que posee funciones polimorficas, herencias, etc.

Diagrama en UML: 

![Proyecto_Final](/uploads/f7fcd8a77a8a77337235c81b232eaaac/Proyecto_Final.jpeg)

Para poder ejecutar el proyecto, compilar: javac Main.java

O en Netbeans ejecutar Main.java
