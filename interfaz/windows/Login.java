package interfaz.windows;

//Librerias
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPanel;

//Paquetes Locales
import interfaz.controllers.LoginController;
import prestamos.persona.hijos.Administrador;
import prestamos.prestamo.Prestamo;

public class Login extends JFrame implements ActionListener {
    //Atributos
        //Atributos Generales
    static final long serialVersionUID = 1;
    private JPanel login, datos, prestamos, admin;
        //Atributos Login
    private JLabel textoCedula, textoPrimerNombre, textoPrimerApellido, textoSegundoApellido;
    private JTextField cedula, primerNombre, primerApellido, segundoApellido;
    private JButton loginButton;
        //Atributos Datos
    private JLabel textoSexo, textoEdad, textoEstrato, textoTelefono;
    private JTextField sexo, edad, estrato, telefono;
    private JButton datosButton;
        //Atributos Prestamos
    private JLabel textoValorPrestamo, textoCuotasPrestamo;
    private JTextField valorPrestamo, cuotasPrestamo;
    private JButton prestamosButton, addPrestamoButton;
        //Atributos Admin
    private JLabel textoCedulaUsuario, textoNombreUsuario, textoTelefonoUsuario;
    private JCheckBox isReported;
    private JButton adminButton;

    // Metodos
    public Login() {
        super();
        this.inicializar();
        this.configurarPaneles();
        this.configurar();
        this.mostrarLogin();
    }

    private void inicializar() {
        this.setSize(310, 310);
        this.setLocationRelativeTo(null);
        this.setLayout(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void configurarPaneles() {
        this.login = new JPanel();
        this.login.setLayout(null);
        this.login.setBounds(0, 0, 310, 310);
        this.datos = new JPanel();
        this.datos.setLayout(null);
        this.datos.setBounds(0, 0, 310, 310);
        this.prestamos = new JPanel();
        this.prestamos.setLayout(null);
        this.prestamos.setBounds(0, 0, 310, 310);
        this.admin = new JPanel();
        this.admin.setLayout(null);
        this.admin.setBounds(0, 0, 310, 310);
    }

    private void configurarLogin() {
        //Inicializacion de Componentes Necesarios
        textoCedula = new JLabel();
        textoPrimerNombre = new JLabel();
        textoPrimerApellido = new JLabel();
        textoSegundoApellido = new JLabel();
        cedula = new JTextField();
        primerNombre = new JTextField();
        primerApellido = new JTextField();
        segundoApellido = new JTextField();
        loginButton = new JButton();

        //Posicionamiento de Componentes
        textoCedula.setText("Cedula");
        textoCedula.setBounds(50, 50, 135, 25);
        textoPrimerNombre.setText("Primer Nombre");
        textoPrimerNombre.setBounds(50, 80, 135, 25);
        textoPrimerApellido.setText("Primer Apelldo");
        textoPrimerApellido.setBounds(50, 110, 135, 25);
        textoSegundoApellido.setText("Segundo Apellido");
        textoSegundoApellido.setBounds(50, 140, 135, 25);
        cedula.setBounds(185, 50, 100, 25);
        primerNombre.setBounds(185, 80, 100, 25);
        primerApellido.setBounds(185, 110, 100, 25);
        segundoApellido.setBounds(185, 140, 100, 25);
        loginButton.setText("Siguiente");
        loginButton.setBounds(50, 230, 200, 30);
        loginButton.addActionListener(this);

        //Agregamiento de Componentes
        this.login.add(textoCedula);
        this.login.add(textoPrimerNombre);
        this.login.add(textoPrimerApellido);
        this.login.add(textoSegundoApellido);
        this.login.add(cedula);
        this.login.add(primerNombre);
        this.login.add(primerApellido);
        this.login.add(segundoApellido);
        this.login.add(loginButton);
    }

    private void configurarDatos() {
        //Inicializacion de Componentes Necesarios
        textoSexo = new JLabel();
        textoEdad = new JLabel();
        textoEstrato = new JLabel();
        textoTelefono = new JLabel();
        sexo = new JTextField();
        edad = new JTextField();
        estrato = new JTextField();
        telefono = new JTextField();
        datosButton = new JButton();

        //Posicionamiento de Componentes
        textoSexo.setText("Sexo");
        textoSexo.setBounds(50, 50, 135, 25);
        textoEdad.setText("Edad");
        textoEdad.setBounds(50, 80, 135, 25);
        textoEstrato.setText("Estrato");
        textoEstrato.setBounds(50, 110, 135, 25);
        textoTelefono.setText("Telefono");
        textoTelefono.setBounds(50, 140, 135, 25);
        sexo.setBounds(185, 50, 100, 25);
        edad.setBounds(185, 80, 100, 25);
        estrato.setBounds(185, 110, 100, 25);
        telefono.setBounds(185, 140, 100, 25);
        datosButton.setText("Siguiente");
        datosButton.setBounds(50, 230, 200, 30);
        datosButton.addActionListener(this);

        //Agregamiento de Componentes
        this.datos.add(textoSexo);
        this.datos.add(textoEdad);
        this.datos.add(textoEstrato);
        this.datos.add(textoTelefono);
        this.datos.add(sexo);
        this.datos.add(edad);
        this.datos.add(estrato);
        this.datos.add(telefono);
        this.datos.add(datosButton);
    }

    private void configurarPrestamos() {
        //Inicializacion de Componentes Necesarios
        textoValorPrestamo = new JLabel();
        textoCuotasPrestamo = new JLabel();
        valorPrestamo = new JTextField();
        cuotasPrestamo = new JTextField();
        prestamosButton = new JButton();
        addPrestamoButton = new JButton();

        //Posicionamiento de Componentes
        textoValorPrestamo.setText("Valor Prestamo");
        textoValorPrestamo.setBounds(50, 50, 135, 25);
        textoCuotasPrestamo.setText("Cuotas");
        textoCuotasPrestamo.setBounds(50, 80, 135, 25);
        valorPrestamo.setBounds(185, 50, 100, 25);
        cuotasPrestamo.setBounds(185, 80, 100, 25);
        prestamosButton.setText("Siguiente");
        prestamosButton.setBounds(50, 230, 200, 30);
        prestamosButton.addActionListener(this);
        addPrestamoButton.setText("Añadir Prestamo");
        addPrestamoButton.setBounds(50, 150, 200, 30);
        addPrestamoButton.addActionListener(this);

        //Agregamiento de Componentes
        this.prestamos.add(textoValorPrestamo);
        this.prestamos.add(textoCuotasPrestamo);
        this.prestamos.add(valorPrestamo);
        this.prestamos.add(cuotasPrestamo);
        this.prestamos.add(prestamosButton);
        this.prestamos.add(addPrestamoButton);
    }

    private void configurarAdmin() {
        //Inicializacion de Componentes Necesarios
        isReported = new JCheckBox("¿Reportado en DataCredito?", false);
        adminButton = new JButton();
        
        //Posicionamiento de Componentes
        isReported.setBounds(0, 140, 300, 30);
        adminButton.setText("Finalizar");
        adminButton.setBounds(50, 230, 200, 30);
        adminButton.addActionListener(this);

        //Agregamiento de Componentes
        this.admin.add(isReported);
        this.admin.add(adminButton);
    }

    public void mostrarLogin() {
        this.setTitle("Login Usuarios");
        this.datos.setVisible(false);
        this.prestamos.setVisible(false);
        this.admin.setVisible(false);
        this.login.setVisible(true);
    }

    public void mostrarDatos() {
        this.setTitle("Rellenado de Datos");
        this.login.setVisible(false);
        this.prestamos.setVisible(false);
        this.admin.setVisible(false);
        this.datos.setVisible(true);
    }

    public void mostrarPrestamos() {
        this.setTitle("Administracion de Prestamos");
        this.login.setVisible(false);
        this.datos.setVisible(false);
        this.admin.setVisible(false);
        this.prestamos.setVisible(true);
    }

    public void mostraraAdmin() {
        this.setTitle("Administrador Clientes");
        this.login.setVisible(false);
        this.datos.setVisible(false);
        this.prestamos.setVisible(false);
        this.agregarInfoUsuario();
        this.admin.setVisible(true);
    }

    private void configurar() {
        //Configurar Paneles
        this.configurarLogin();
        this.configurarDatos();
        this.configurarPrestamos();
        this.configurarAdmin();

        //Añadir Paneles
        this.add(this.login);
        this.add(this.datos);
        this.add(this.prestamos);
        this.add(this.admin);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.loginButton) {
            LoginController.administrador = new Administrador(Long.parseLong(cedula.getText()), primerNombre.getText(), primerApellido.getText(), segundoApellido.getText());
            this.mostrarDatos();
        } else if(e.getSource() == this.datosButton) {
            LoginController.administrador.setNewCliente(sexo.getText().charAt(0), Integer.parseInt(edad.getText()), Long.parseLong(telefono.getText()));
            this.mostrarPrestamos();
        } else if(e.getSource() == this.addPrestamoButton) {
            if(Long.parseLong(valorPrestamo.getText()) > LoginController.administrador.VALORMAXIMOPRESTAR) {
                JOptionPane.showMessageDialog(null, "Los prestamos no pueden ser mayores a\n" + Prestamo.imprimirPrecio(LoginController.administrador.VALORMAXIMOPRESTAR));
            } else if(Integer.parseInt(cuotasPrestamo.getText()) > 6) {
                JOptionPane.showMessageDialog(null, "El maximo de cuotas es de 6");
            }
            else {
                LoginController.administrador.getCliente().setNewPrestamo(Long.parseLong(valorPrestamo.getText()), Integer.parseInt(cuotasPrestamo.getText()));
                JOptionPane.showMessageDialog(null, "Prestamo Agregado Correctamente");
            }
        } else if(e.getSource() == this.prestamosButton) {
            this.mostraraAdmin();
        } else {
            LoginController.administrador.validacionCliente(this.isReported.isSelected());
            LoginController.administrador.getCliente().mostrar();
            JOptionPane.showMessageDialog(null, "Saliendo");
            this.setVisible(false);
            this.dispose();
        }
    }

    //Agregar Informacion a la petaña Admin
    private void agregarInfoUsuario() {
        //Inicializacion de Componentes Necesarios
        textoCedulaUsuario = new JLabel();
        textoNombreUsuario = new JLabel();
        textoTelefonoUsuario = new JLabel();
        //Posicionamiento de Componentes
        textoCedulaUsuario.setText("Cedula: " + LoginController.administrador.getCliente().getCedula());
        textoCedulaUsuario.setBounds(0, 50, 300, 25);
        textoNombreUsuario.setText("Nombre: " + LoginController.administrador.getCliente().getPrimerNombre() + " " + LoginController.administrador.getCliente().getPrimerApellido() + " " + LoginController.administrador.getCliente().getSegundoApellido());
        textoNombreUsuario.setBounds(0, 80, 300, 25);
        textoTelefonoUsuario.setText("Telefono: " + LoginController.administrador.getCliente().getTelefono());
        textoTelefonoUsuario.setBounds(0, 110, 300, 25);
        //Agregamiento de Componentes
        this.admin.add(textoCedulaUsuario);
        this.admin.add(textoNombreUsuario);
        this.admin.add(textoTelefonoUsuario);
    }

}
